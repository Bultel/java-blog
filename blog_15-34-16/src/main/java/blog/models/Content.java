package blog.models;

import java.time.ZonedDateTime;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class Content {

	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private long id;
	
	private ZonedDateTime date;
	
	@ManyToOne
	private User author;
	
	@OneToMany(mappedBy="target")
	private Set<Comment> comments;

}
