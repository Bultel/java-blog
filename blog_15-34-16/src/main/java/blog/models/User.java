package blog.models;

import java.util.Set;

import org.hibernate.annotations.Where;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private long id;
	
	private String pseudo;
	
	private String password;
	
	private String email;
	
	@OneToMany(mappedBy="author", targetEntity = Content.class)
	@Where(clause = "DTYPE = 'Article'")
	private Set<Article> articles;
	
	@OneToMany(mappedBy="author", targetEntity = Content.class)
	@Where(clause = "DTYPE = 'Comment'")
	private Set<Comment> comments;
	
}
