package blog.models;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public abstract class Article extends Content {

	private String title;
	
	private ArticleType type;
	
	@ManyToMany
	private Set<Category> categories;
}
