package blog;

import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import blog.models.User;


public class App {

	public static void main(String[] args) {
		var sf = new Configuration().configure().buildSessionFactory();
		Transaction t = null;
		try (var s = sf.openSession()) {
			t = s.beginTransaction();
			s.persist(User.builder().pseudo("andre").build());
			
			t.commit();
		} catch (Exception e) {
			if (t != null) t.rollback();
			throw e;
		}
	}

}
